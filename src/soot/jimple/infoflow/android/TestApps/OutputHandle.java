package soot.jimple.infoflow.android.TestApps;

import java.util.HashMap;

import soot.Scene;
import soot.SootClass;
import soot.jimple.infoflow.results.ResultSinkInfo;
import soot.jimple.infoflow.results.ResultSourceInfo;
import handleFlowDroid.DataFeatureSpace;
import handleFlowDroid.OutputPreparations;


public class OutputHandle {
	public static DataFeatureSpace findCategory(ResultSourceInfo source , ResultSinkInfo sink, DataFeatureSpace data){
		String strSrcCat = mapCategory("source", source , sink);
		String strSnkCat = mapCategory("sink", source , sink); 
		if (!strSnkCat.equals("") && !strSrcCat.equals(""))
			data.addValue(strSrcCat, strSnkCat);
		System.out.println("\t" + strSrcCat + " -> " + strSnkCat);
		return data;
	}
	
	private static String mapCategory(String srcOrSnk, ResultSourceInfo source , ResultSinkInfo sink){
		HashMap<String, String> categoryApiHashMap = OutputPreparations.getCategoryAPImap();
		
		SootClass sootSuperClass = null;
		String strMethod = "";
		String strCustomSignature = "";
		String strCat = "";				
		String strClass;
		if (source.toString().contains("@parameter") || sink.toString().contains("@parameter")){
			System.out.println("\t<<< Just Parameter >>>");
			return strCat; 
		} 
		
		if (srcOrSnk.equals("source")){ 
			strMethod = source.getSource().toString().substring(
					source.getSource().toString().indexOf(":"), source.getSource().toString().indexOf(">")+1);
			strClass = source.toString().substring(source.toString().indexOf("<")+1, source.toString().indexOf(":"));			
		}else{
			strMethod = sink.getSink().toString().substring(
					sink.getSink().toString().indexOf(":"), sink.getSink().toString().indexOf(">")+1);
			strClass = sink.toString().substring(sink.toString().indexOf("<")+1, sink.toString().indexOf(":"));	
		}
		sootSuperClass = Scene.v().loadClass(strClass, SootClass.HIERARCHY);

		boolean flgFound = false;
		while (!sootSuperClass.getName().equals("java.lang.Object")){
				
			strCustomSignature = "<".concat(strClass.concat(strMethod));
			if (categoryApiHashMap.containsKey(strCustomSignature.replaceAll("\\s","")))
			{
				flgFound = true;
				strCat = categoryApiHashMap.get(strCustomSignature.replaceAll("\\s",""));
				break;
			}
			sootSuperClass = sootSuperClass.getSuperclass();
			strClass = sootSuperClass.getName();
		}
		if (!flgFound) {
			//System.out.println("<<< NO_CATEGORY >>>");
			strCat = "NO_CATEGORY";
		}
		return strCat;
		
	}
}
